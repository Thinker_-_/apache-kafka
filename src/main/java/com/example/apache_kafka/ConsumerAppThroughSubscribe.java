package com.example.apache_kafka;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.example.apache_kafka.config.ConsumerConfig;

public class ConsumerAppThroughSubscribe {
	
	
	public static void main(String args[]){
		
		Scanner sc=new Scanner(System.in);
		Properties consumerConfiguration=new Properties();
		consumerConfiguration.put(ConsumerConfig.CONFIG_KEY_BOOTSTRAP_SERVERS, "localhost:9093");
		consumerConfiguration.put(ConsumerConfig.CONFIG_KEY_KEY_DESERIALIZER, ConsumerConfig.CONFIG_VALUE_KEY_DESERIALIZER);
		consumerConfiguration.put(ConsumerConfig.CONFIG_KEY_VALUE_DESERIALIZER, ConsumerConfig.CONFIG_VALUE_VALUE_DESERIALIZER);
		consumerConfiguration.put(ConsumerConfig.CONFIF_KEY_AUTO_OFFSET_RESET, ConsumerConfig.CONFIF_VALUE_AUTO_OFFSET_RESET);
		consumerConfiguration.put("group.id","test");
		KafkaConsumer<String, String> myConsumer=new KafkaConsumer<String, String>(consumerConfiguration);
		List<String> topics=new ArrayList<String>();
		topics.add("my_replicatied");
		myConsumer.subscribe(topics);
		System.out.println(myConsumer.listTopics());
		try{
			/**
			 * Inside poll loop
			 */
			while(true){
				ConsumerRecords<String, String> records=myConsumer.poll(100);
				for(ConsumerRecord<String, String> record:records){
					System.out.println(String.format("topic %s,partition %d,key %s,message %s", record.topic(),record.partition(),record.key(),record.value()));
				}
			}
			}catch(Exception e){
			e.printStackTrace();
		}finally {
			System.out.println("Inside finally block");
			myConsumer.unsubscribe();
			myConsumer.close();
			sc.close();
		}
	}

}
