package com.example.apache_kafka.config;

public class ConsumerConfig {
	public static String CONFIG_KEY_BOOTSTRAP_SERVERS="bootstrap.servers";
	public static String CONFIG_KEY_KEY_DESERIALIZER="key.deserializer";
	public static String CONFIG_KEY_VALUE_DESERIALIZER="value.deserializer";
	public static String CONFIG_VALUE_BOOTSTRAP_SERVERS="localhost:9092,localhost:9093";
	public static String CONFIG_VALUE_KEY_DESERIALIZER="org.apache.kafka.common.serialization.StringDeserializer";
	public static String CONFIG_VALUE_VALUE_DESERIALIZER="org.apache.kafka.common.serialization.StringDeserializer";
	public static String CONFIF_KEY_AUTO_OFFSET_RESET="auto.offset.reset";
	public static String CONFIF_VALUE_AUTO_OFFSET_RESET="earliest";
	
}
