package com.example.apache_kafka.config;

public final class ProducerConfig {
	
	public static String CONFIG_KEY_BOOTSTRAP_SERVERS="bootstrap.servers";
	public static String CONFIG_KEY_KEY_SERIALIZER="key.serializer";
	public static String CONFIG_KEY_VALUE_SERIALIZER="value.serializer";
	public static String CONFIG_VALUE_BOOTSTRAP_SERVERS="localhost:9092,localhost:9093";
	public static String CONFIG_VALUE_KEY_SERIALIZER="org.apache.kafka.common.serialization.StringSerializer";
	public static String CONFIG_VALUE_VALUE_SERIALIZER="org.apache.kafka.common.serialization.StringSerializer";
	public static String CONFIG_KEY_ACKS="acks";
	public static String CONFIG_VALUE_ACKS="all";
	public static String PARTITION_NAME="my_topics";	
}
