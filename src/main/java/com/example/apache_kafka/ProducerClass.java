package com.example.apache_kafka;

import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.example.apache_kafka.config.ProducerConfig;

/**
 * KafkaProducer demo
 */

public class ProducerClass 
{
    public static void main( String[] args )
    {
    	Properties producer_configuration=new Properties();
    	producer_configuration.put(ProducerConfig.CONFIG_KEY_BOOTSTRAP_SERVERS, ProducerConfig.CONFIG_VALUE_BOOTSTRAP_SERVERS);
    	producer_configuration.put(ProducerConfig.CONFIG_KEY_KEY_SERIALIZER, ProducerConfig.CONFIG_VALUE_KEY_SERIALIZER);
    	producer_configuration.put(ProducerConfig.CONFIG_KEY_VALUE_SERIALIZER, ProducerConfig.CONFIG_VALUE_VALUE_SERIALIZER);
    	producer_configuration.put(ProducerConfig.CONFIG_KEY_ACKS, ProducerConfig.CONFIG_VALUE_ACKS);
    	
    	KafkaProducer<String, String> myProducer=new KafkaProducer<String, String>(producer_configuration);
   		Scanner sc=new Scanner(System.in);
   	
    	try{
    	while(true){
     		String message=sc.nextLine();
    		Future<RecordMetadata> result= myProducer.send(new ProducerRecord<String, String>("my_replicatied", message));
    	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally {
			myProducer.close();
			sc.close();
		}
    }
}
