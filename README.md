**Introduction**

This is sample POC demonstrating cluster configuration of four broker nodes,along with a zookeeper server, which is managing the health of cluster and a single topic with three partition and a replication factor of three. 

*I recommend that you should first go through apache-kafka architecture design before continuing this POC.*


## Step to start the Cluster


You�ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Start the zookeeper first.
	./bin/zookeeper-server-start.sh config/zookeeper.properties 

2. After that start the all broker nodes with same configuration inside ClusterConfiguration folder.
   ./bin/kafka-server-start.sh config/server.properties 
   ./bin/kafka-server-start.sh config/server2.properties 
   ./bin/kafka-server-start.sh config/server1.properties 
   ./bin/kafka-server-start.sh config/server3.properties

---

## Steps to create partition

1.  ./bin/kafka-topics.sh --create --topic my_replicatied --zookeeper localhost:2181 --replication-factor 3 --partitions 3

You can view the configuration of topic via describe command of kafka-topics.sh shell script file provided in kafka-bundle.

2. ./bin/kafka-topics.sh --describe --topic my_replicatied --zookeeper localhost:2181 

## Start the producer and produce some messages.
Import the project into any standard IDE(Eclipse , STS,IntelJ etc)

Run the ProducerClass to produces messages.

  
## Start the Consumer

 In apache-kafka a Consumer is also a kind of producer , producing the offset and commiting them on to **__consumer_offsets** topic.Its a topic where apache-kafka holds the information regarding the message read to 
 far by a consumer.
 
 Run the consumer a normal java application.
 
 Note: You can view the configuration of **__consumer_offsets** topic through following command.
 
 ./bin/kafka-topics.sh --describe --topic __consumer_offsets --zookeeper localhost:2181 